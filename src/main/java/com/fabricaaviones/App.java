package com.fabricaaviones;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println("*******************AVION DE CARGA**********************");
        AvionCarga avionCarga = new AvionCarga("Gris", 50);

        System.out.println("Atributos del Avión");
        System.out.println("Color: "+avionCarga.getColor());
        System.out.println("Tamaño: "+avionCarga.getTamanio()+" mts");

        System.out.println("*************************************");
        avionCarga.cargar();
        avionCarga.despegar();
        avionCarga.aterrizar();
        avionCarga.frenar();
        avionCarga.descargar();

        System.out.println("*******************AVION PASAJEROS**********************");
        AvionPasajeros avionPasajeros = new AvionPasajeros(50, "Blanco", 40);

        avionPasajeros.despegar();
        avionPasajeros.servir();
        avionPasajeros.aterrizar();
        avionPasajeros.frenar();

        System.out.println("*******************AVION MILITAR**********************");
        AvionMilitar avionMilitar = new AvionMilitar(2, "Azul", 10);
        avionMilitar.despegar();
        avionMilitar.identificar_objetivo(false);
        avionMilitar.identificar_objetivo(true);
        avionMilitar.identificar_objetivo(true);
        avionMilitar.identificar_objetivo(true);
        avionMilitar.aterrizar();
        avionMilitar.frenar();
    }
}
