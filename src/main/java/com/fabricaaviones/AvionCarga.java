package com.fabricaaviones;

public class AvionCarga extends Avion {
    
    //CONSTRUCTOR
    public AvionCarga(String color, double tamanio){
        super(color, tamanio);
    }

    //ACCIONES
    public void cargar(){
        System.out.println("Cargando...");
    }

    public void descargar(){
        System.out.println("Descargando...");
    }
}
