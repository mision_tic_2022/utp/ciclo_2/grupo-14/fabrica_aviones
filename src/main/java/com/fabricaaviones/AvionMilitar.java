package com.fabricaaviones;

public class AvionMilitar extends Avion {

    private int misiles;

    public AvionMilitar(int misiles, String color, double tamanio){
        super(color, tamanio);
        this.misiles = misiles;
    }

    private void disparar(){
        if(misiles > 0){
            System.out.println("tatatatatata");
            misiles-=1;
        }else{
            System.out.println("Municion agotada");
        }
        
    }

    public void identificar_objetivo(boolean objetivo){
        if(objetivo){
            disparar();
        }else{
            System.out.println("No es objetivo militar");
        }
    }
    
}
