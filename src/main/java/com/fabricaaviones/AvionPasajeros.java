package com.fabricaaviones;

public class AvionPasajeros extends Avion {

    private int pasajeros;

    public AvionPasajeros(int pasajeros, String color, double tamanio){
        super(color, tamanio);
        this.pasajeros = pasajeros;
    }

    public int getPasajeros(){
        return pasajeros;
    }

    public void setPasajeros(int pasajeros){
        this.pasajeros = pasajeros;
    }

    public void servir(){
        System.out.println("Sirviendo alimentos a los pasajeros...");
    }
    
}
